class Array
  def to_histogram
    histogram = inject(Hash.new(0)) { |array_to_hash, array_object| array_to_hash[array_object] += 1; array_to_hash }
  end
end

arr = [1,2,2,3,3,4,4,4,5,5]
puts arr.to_histogram
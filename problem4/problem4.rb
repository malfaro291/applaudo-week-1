file_name = "strings.txt"
if File.exist? file_name

  doc = open("strings.txt") do |f| 
    f.read.gsub(/.\s+/) do |c|
      if c == ".\n" || c == ".\n\n" || c == ":\n\n"
        c
      else
        c.strip + " "
      end
    end
  end
  
  modified_text = doc.gsub(" . ", ". ").gsub(". .", ". ")
 
  open("modified_strings.txt", "w") { |f| f.puts modified_text }

  words = modified_text.split(/\s+/)

  dis_counter = 0
  ing_counter = 0

  words.each do |word|
    word = word.downcase
    dis_match = Regexp.compile('dis+\w').match(word) 
    ing_match = Regexp.compile('\w+ing').match(word)
    if dis_match
      dis_counter += 1
    end
    if ing_match
      ing_counter += 1
    end
  end

  puts "Total of words that starts with 'dis-': #{ dis_counter.to_s}"
  puts "Total of words that ends with '-ing': #{ing_counter.to_s}"

else
  puts "#{file_name} doesn't exist"
end

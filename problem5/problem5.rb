require 'csv'
require 'time'
ENV['TZ'] = "America/New_York"

def convert_to_est(date)
  date = Time.parse(date)
  date.to_s
rescue ArgumentError
  false
end

if File.exist? "events.csv"
  valid_dates_counter = 0
  valid_dates = []
  line = 1

  if File.exist? "invalid_events.csv"
    File.delete "invalid_events.csv"
  end
  if File.exist? "valid_events.csv"
    File.delete "valid_events.csv"
  end

  CSV.foreach("events.csv") do |row|
    if !convert_to_est(row[1])
      open("invalid_events.csv", "a") do |f|
        f.puts "#{row[0]}, Invalid Date in line #{line.to_s}"
      end
      puts "Invalid Date in line #{line.to_s}"
    else
      valid_dates << [row[0], convert_to_est(row[1])]
    end
    line += 1
  end

  valid_dates.sort_by! { |event| event[1] }

  open("valid_events.csv", "a") do |f|
    valid_dates.each { |event| f.puts "#{event[0]},#{event[1]}"  }
  end
else
  puts "File doesn't exist"
end  
def mean(array)
  array_mean = array.inject(0) { |sum, x| sum += x }
  array_mean/array.size
end

def median(array)
  return nil if array.empty?
  array = array.sort
  m_pos = array.size / 2
  return array.size % 2 == 1 ? array[m_pos] : mean(array[m_pos-1..m_pos])
end

def modes(array, find_all=true)
  histogram = array.inject(Hash.new(0)) { |array_to_hash, array_object| array_to_hash[array_object] += 1; array_to_hash }
  modes = nil
  histogram.each_pair do |item, times|
    modes << item if modes && times === modes[0] and find_all
    modes = [times, item] if (!modes && times > 1) or (modes && times>modes[0])
  end
  return modes ? modes[1..modes.size] : modes
end

arr = [2,3,1,2,2,3,4,5,3,2,3].collect { |value| value.to_f }
if !arr.empty?
  p arr
  puts "Mean:#{mean(arr).to_s}"
  puts "Median: #{median(arr).to_s}"
  puts "Modes: #{modes(arr).to_s}"
end